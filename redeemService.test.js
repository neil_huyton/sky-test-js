const constants = require("./constants");
const redeemService = require("./redeemService");

describe("Eligibility", () => {
  const customerAccountNumber = 123;
  const { channels, eligibility } = constants;

  test("Customer is eligible, returns sports, kids, music, news, movies rewards", () => {
    const portfolio = [
      channels.SPORTS,
      channels.KIDS,
      channels.MUSIC,
      channels.NEWS,
      channels.MOVIES
    ];
    const eligibilityService = jest.fn(() => eligibility.CUSTOMER_ELIGIBLE);

    const testRedeemService = redeemService({
      customerAccountNumber,
      portfolio,
      eligibilityService
    });
    expect(testRedeemService).toEqual({
      data: [
        "CHAMPIONS_LEAGUE_FINAL_TICKET",
        "KARAOKE_PRO_MICROPHONE",
        "PIRATES_OF_THE_CARIBBEAN_COLLECTION"
      ],
      error: "",
      status: 200
    });
  });

  test("Customer is eligible, returns sports and kids rewards", () => {
    const portfolio = [channels.SPORTS, channels.KIDS];
    const eligibilityService = jest.fn(() => eligibility.CUSTOMER_ELIGIBLE);

    const testRedeemService = redeemService({
      customerAccountNumber,
      portfolio,
      eligibilityService
    });
    expect(testRedeemService).toEqual({
      data: ["CHAMPIONS_LEAGUE_FINAL_TICKET"],
      error: "",
      status: 200
    });
  });

  test("Customer is eligible, returns kids and news rewards", () => {
    const portfolio = [channels.KIDS, channels.NEWS];
    const eligibilityService = jest.fn(() => eligibility.CUSTOMER_ELIGIBLE);

    const testRedeemService = redeemService({
      customerAccountNumber,
      portfolio,
      eligibilityService
    });
    expect(testRedeemService).toEqual({
      data: [],
      error: "",
      status: 200
    });
  });

  test("Customer is ineligible, returns no rewards", () => {
    const portfolio = [];
    const eligibilityService = jest.fn(() => eligibility.CUSTOMER_INELIGIBLE);

    let testRedeemService = redeemService({
      customerAccountNumber,
      portfolio,
      eligibilityService
    });
    expect(testRedeemService).toEqual({ data: [], error: "", status: 200 });
  });

  test("Technical failure exception, returns no rewards", () => {
    const portfolio = [];
    const eligibilityService = jest.fn(() => 500);

    let testRedeemService = redeemService({
      customerAccountNumber,
      portfolio,
      eligibilityService
    });
    expect(testRedeemService).toEqual({
      data: [],
      error: "Technical error",
      status: 500
    });
  });

  test("Invalid account number exception, return no rewards and notifies the client that the account number is invalid ", () => {
    const portfolio = [];
    const eligibilityService = jest.fn(() => 400);

    let testRedeemService = redeemService({
      customerAccountNumber,
      portfolio,
      eligibilityService
    });
    expect(testRedeemService).toEqual({
      data: [],
      error: "Account number is invalid",
      status: 400
    });
  });
});
