const eligibility = {
  CUSTOMER_ELIGIBLE: "CUSTOMER_ELIGIBLE",
  CUSTOMER_INELIGIBLE: "CUSTOMER_INELIGIBLE"
};

const channels = {
  SPORTS: "SPORTS",
  KIDS: "KIDS",
  MUSIC: "MUSIC",
  NEWS: "NEWS",
  MOVIES: "MOVIES"
};

const rewards = {
  SPORTS: "CHAMPIONS_LEAGUE_FINAL_TICKET",
  KIDS: "N/A",
  MUSIC: "KARAOKE_PRO_MICROPHONE",
  NEWS: "N/A",
  MOVIES: "PIRATES_OF_THE_CARIBBEAN_COLLECTION"
};

module.exports = { channels, eligibility, rewards };
