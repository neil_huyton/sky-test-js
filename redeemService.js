const constants = require("./constants");

function redeemService({
  customerAccountNumber,
  portfolio,
  eligibilityService
}) {
  const { eligibility, rewards } = constants;
  const result = eligibilityService(customerAccountNumber);
  const data = [];
  let status = 200;
  let error = "";

  if (result === eligibility.CUSTOMER_ELIGIBLE) {
    portfolio.forEach(item => {
      rewards[item] !== "N/A" ? data.push(rewards[item]) : null;
    });
  }

  if (result === 500) {
    status = 500;
    error = "Technical error";
  }

  if (result === 400) {
    status = 400;
    error = "Account number is invalid";
  }

  return { data, status, error };
}

module.exports = redeemService;
